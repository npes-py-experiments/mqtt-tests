# mqtt-tests

testing paho-mqtt

1. Clone the repository `git clone git@gitlab.com:npes-py-experiments/mqtt-tests.git` 
2. Create a virtual environment https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment 
2. Activate the virtual environment https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#activating-a-virtual-environment
3. Install requirements from requirements.txt https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#using-requirements-files
4. Run `python3 app.py` (linux) or `py app.py` (windows)
